const User  = require("../models/User");
const Product = require("../models/Product");
const objectId = require("mongodb").ObjectId;

const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.signUp = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        userName: reqBody.userName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    })

    return User.find({$or: [{userName: reqBody.userName}, {email: reqBody.email}]}).then(result => {
        if(result == ""){
            return newUser.save().then((success, err) => {
                if(err){
                    return false;
                }
                else{
                    return true;
                }
            });
        }
        else{
            return "Email or Username already exist"
        }
    })
};

module.exports.login = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return "You don't have an account yet"
        }
        else{
            const passwordMatch = bcrypt.compareSync(reqBody.password, result.password);

            if(passwordMatch){
                return{access: auth.accessToken(result)};
            }
            else{
                return "Your password is incorrect"
            }
        }
    })
};


module.exports.updateStatusAdmin = (userId, reqBody) => {
    let updatedStatus = {
        isAdmin: reqBody.isAdmin
    }

    return User.findByIdAndUpdate(userId, updatedStatus).then((success, err) => {
        if(err){
            return false;
        }
        else{
            return "Successfully Updated Status!"
        }
    })
};

module.exports.orders = async (data) => {

    let productPrice = await Product.findById(data.productId).then(product => {
        return product.price;
    });

    let onSale = await Product.findById(data.productId).then(product => product);

    let updateProduct = await Product.findById(data.productId).then(product => {
        
        if(product.stocks < data.quantity){
            return false;
        }

        else {
            product.stocks = product.stocks - data.quantity;
            product.usersOrder.push({userId: data.userId, quantity: data.quantity});

            User.findById(data.userId).then(user => {
                if(onSale.onSale == true){
                   
                    let initial = data.quantity * productPrice;

                    let percent = (onSale.percentOff / 100) * (data.quantity * productPrice);
                    
                    user.totalOrderAmount = user.totalOrderAmount + (initial - percent);
                    // console.log(user.totalOrderAmount);
                    return user.save().then((success, err) => {
                        if(err){
                            return false;
                        }
                        else{
                            return true;
                        }
                        })
                    }
                
                else{
                    user.totalOrderAmount = user.totalOrderAmount + (data.quantity * productPrice);
        
                    return user.save().then((success, err) => {
                    if(err){
                        return false;
                    }
                    else{
                        return true;
                    }
                    })
                }
            })
        }
        return product.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
        })
    });

    let updateUser = await User.findById(data.userId).then(user => {

        if(updateProduct){
        user.orders.push({productId: data.productId, quantity: data.quantity});

        return user.save().then((success, err) => {
            if(err){
                return false;
            }
            else{
                return true;
            }
            })
        }
        else{
            return false;
        }
    })

    if(updateUser && updateProduct){
        return "Successfully placed an order"
    }
    else{
        return false
    }
};

module.exports.getUserOrders = (userId) => {
    return User.aggregate([{$match: {"_id": objectId(userId)}}, {$project: {"_id": 0, "firstName": 0, "lastName": 0, "userName": 0, "email": 0, "isAdmin": 0, "password": 0, "mobileNo": 0, "createdOn": 0, "__v": 0}}]).then(result => result);

    // return User.findById(userId).then(result => result.orders);
};

module.exports.getAllUsersOrders = () => {
    return User.aggregate([{$match: {"isAdmin": false}},{$project: {"userName": 0, "isAdmin": 0, "password": 0, "mobileNo": 0, "createdOn": 0, "__v": 0}}]).then(result => result);

};

module.exports.getSpecificOrders = (reqBody) => {
    return User.aggregate([{$match: {"_id": objectId(reqBody._id)}}, {$project: {"userName": 0, "isAdmin": 0, "password": 0, "createdOn": 0, "__v": 0}}]).then(result => result);
};


module.exports.updateInfo = (data, reqBody) => {
    let updateInfo = {
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        userName: reqBody.userName,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    }

    return User.find({$or: [{"userName": updateInfo.userName}, {"email": updateInfo.email}]}).then(result => {
            if(result == "" || result == null){
                return User.findByIdAndUpdate(data.userId, updateInfo).then((success, err) => {
                    if(err){
                        return false;
                    }
                    else{
                        return "Successfully updated your info"
                    }
                })
            }
            else{
                return "The username or email already exist";
            }
        })
};

