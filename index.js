const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");



// Server
const app = express();
const port = 4000;

// Database Connection
mongoose.connect("mongodb+srv://admin:admin@coursebooking.wc3mbgj.mongodb.net/Ecommerce-app?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Connection notification
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connnection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// API routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);



// Port
app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port ${process.env.PORT || port}`);
});