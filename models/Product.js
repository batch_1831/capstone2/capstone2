const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "The name of the product is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    stocks: {
        type: Number,
        required: [true, "Number of stocks is required"]
    },
    category: {
        type: String,
        required: [true, "Category is required"]
    },
    onSale: {
        type: Boolean,
        default: false
    },
    percentOff: {
        type:  Number,
        default: 0
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    usersOrder: [{
        userId: {
            type: String,
            required: [true, "User id is required"]
        },
        orderedOn: {
            type: Date,
            default: new Date()
        },
        quantity: {
            type: Number,
            default: 1
        }
    }]
});

module.exports = mongoose.model("Product", productSchema);