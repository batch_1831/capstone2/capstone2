const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
     lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    userName:{
        type: String,
        required: [true, "Username is required"]
    },
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is requied"]
    },
    createdOn:{
        type: Date,
        default: new Date()
    },
    orders: [
        {
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        },
        quantity: {
            type: Number,
            default: 1
        },
        orderedOn: {
            type: Date,
            default: new Date()
        }
    }],
    totalOrderAmount: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model("User", userSchema);