const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

router.post("/signup", (req, res) => {
    userControllers.signUp(req.body).then(result => res.send(result));
});

router.post("/login", (req, res) => {
    userControllers.login(req.body).then(result => res.send(result));
});

router.patch("/:userId/updateStatusAdmin", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.updateStatusAdmin(req.params.userId, req.body).then(result => res.send(result));
    }
    else{
        res.send("You don't have the access on this page");
    }
});



router.post("/orders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId: userData.id,
        productId: req.body.productId,
        quantity: req.body.quantity
    }


    if(userData.isAdmin){
        res.send("You dont have access on this page");
    }
    else{
        userControllers.orders(data).then(result => res.send(result));
    }
});

router.get("/:userId/orders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        res.send("You don't have permission on this page")
    }
    else{
        userControllers.getUserOrders(req.params.userId).then(result => res.send(result));
    }
});

router.get("/userOrders", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.getAllUsersOrders().then(result => res.send(result));
    }
    else{
        res.send ("You don't have access on this page");
    };
    
});

router.get("/userSpecificOrders", auth.verify, (req, res) =>{
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userControllers.getSpecificOrders(req.body).then(result => res.send(result));
    }
    else{
        res.send("You don't have permission on this page");
    }
});


router.put("/info", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    let data = {
        userId: userData.id
    }

    if(userData.isAdmin){
        res.send("You don't have access on this page");
    }
    else{
        userControllers.updateInfo(data, req.body).then(result => res.send(result));
    }
});

module.exports = router;